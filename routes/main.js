const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
const path = require('path');
const User = require('./../models/userModel')

const tokenList = {};
const router = express.Router();

router.get('/status', (req, res, next) => {
  res.status(200).json({ status: 'ok' });
});

router.post('/signup', passport.authenticate('signup', { session: false }), async (req, res, next) => {
  res.status(200).json({ message: 'signup successful' });
});

router.post('/login', async (req, res, next) => {
  passport.authenticate('login', async (err, user, info) => {
    try {
      if (err || !user) {
        const error = new Error('An Error occured');
        return next(error);
      }
      req.login(user, { session: false }, async (error) => {
        if (error) return next(error);
        const body = {
          _id: user._id,
          email: user.email
        };

        const token = jwt.sign({ user: body }, 'top_secret', { expiresIn: 300 });
        const refreshToken = jwt.sign({ user: body }, 'top_secret_refresh', { expiresIn: 86400 });
        const asset = user.asset;

        // store tokens in cookie
        res.cookie('jwt', token);
        res.cookie('refreshJwt', refreshToken);
        res.cookie('asset', asset);

        // store tokens in memory
        tokenList[refreshToken] = {
          token,
          refreshToken,
          email: user.email,
          _id: user._id
        };

        //Send back the token to the user
        return res.status(200).json({ token, refreshToken });
      });
    } catch (error) {
      return next(error);
    }
  })(req, res, next);
});

router.post('/token', (req, res) => {
  const { refreshToken } = req.body;
  if (refreshToken in tokenList) {
    const body = { email: tokenList[refreshToken].email, _id: tokenList[refreshToken]._id };
    const token = jwt.sign({ user: body }, 'top_secret', { expiresIn: 300 });

    // update jwt
    res.cookie('jwt', token);
    tokenList[refreshToken].token = token;

    res.status(200).json({ token });
  } else {
    res.status(401).json({ message: 'Unauthorized' });
  }
});

router.get('/getUserInfo', (req, res, next) => {
  res.status(200).json({asset: req.cookies.asset});
})

//change asset for a particular user
router.post('/changeOwnership', (req, res, next) => {
  if((req.body.username || req.body.assetId) == '') {
    console.log("Values missing for changing asset owner");
    res.status(404).send("Missing values in post request");
  }
  let asset = req.body.assetId;
  let filter = {name: req.body.username};
  let update = { asset : asset};
  User.findOneAndUpdate(filter, update, (err, data) => {
    if (err) 
      res.send(err);
    res.send(data)
  } );
  //update the cookie with the current value too
  res.cookie('asset', asset);
})

// router.post('/logout', (req, res) => {
//   if (req.cookies) {
//     const refreshToken = req.cookies['refreshJwt'];
//     if (refreshToken in tokenList) delete tokenList[refreshToken]
//     res.clearCookie('refreshJwt');
//     res.clearCookie('jwt');
//     res.clearCookie('asset');
//   }
//   res.redirect('/');
//   // return res.sendFile(path.join(__dirname + './../public/index.html'));
//   // res.status(200).json({ message: 'logged out' });
// });

router.get('/logout', function(req, res, next) {
  console.log("coming in for logout")
  if (req.cookies) {
    const refreshToken = req.cookies['refreshJwt'];
    if (refreshToken in tokenList) delete tokenList[refreshToken]
    res.clearCookie('refreshJwt');
    res.clearCookie('jwt');
    res.clearCookie('asset');
  }
  res.redirect('../');
});

module.exports = router;
